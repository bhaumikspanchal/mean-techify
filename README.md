# Mean Techify

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.0.1.

## Development server

Run `npm start` for a dev server. Navigate to `http://localhost:3000/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## Here is the list for test users
Admin : email : bhaumik.panchal@example.com password : Bhaumik@1975
editor : email : rolf.hegdal@example.com password : Rolf@1975
        email : victoria.chavez85@example.com password : Victoria@1975