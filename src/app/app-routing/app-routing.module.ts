import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from '../login/login.component';
import { UsersComponent } from 'app/users/users.component';
import { UserProfileComponent } from 'app/user-profile/user-profile.component';
import { AuthGuard } from 'app/auth-gaurd';


const routes: Routes = [
  {
    path: '',
    component: LoginComponent
  },
  {
    path: 'admindashboard',
    component: UsersComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'users/:_id',
    component: UserProfileComponent,
    canActivate: [AuthGuard],
  }
]

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
