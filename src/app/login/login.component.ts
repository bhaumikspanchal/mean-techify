import { Component, OnInit } from '@angular/core';
import { UserService } from 'app/userservice.service';
import { Router } from '@angular/router';
import { LocalStorageService } from 'angular-2-local-storage';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [ UserService ]
})
export class LoginComponent implements OnInit {


  email: any;
  password: any;
  loading: any =  false;
  authError: any = { auth: true};

  passwordPattern = '^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*]).{8,}';
  constructor(
    public userService: UserService,
    private router: Router,
    public localStorage: LocalStorageService
  ) {

  }

  ngOnInit() {
  }

  login() {
    this.loading = true;
    this.authError = {}
    this.userService.authenticateUser({email: this.email, password: this.password})
      .then(res=>{
        this.loading = false;
        // console.log('login response', res);
        this.localStorage.set('token', res.token);
        if( res.isAdmin ) {
          this.router.navigate(['/admindashboard'])
        } else {
          this.router.navigate(['/users', res._id]);
        }
      })
      .catch(err => {
        this.authError = err;
        this.loading = false;
        setTimeout(()=>{
          this.authError = { auth: true};
        }, 2000);
        // console.log('login error', err);
      })

  }
}
