import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LocalStorageService } from 'angular-2-local-storage';
import { UserService } from 'app/userservice.service';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css'],
  providers: [ UserService ]
})
export class UserProfileComponent implements OnInit {
  public user: any;
  errorMessage: string;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private userService: UserService,
    private localStorageService: LocalStorageService) {
    this.user = {
      name: '',
      dob: '',
      location: ''
    }
  }

  ngOnInit() {
    this.getUser(this.route.snapshot.params._id)
  }

  onLogout() {
    this.localStorageService.remove('token');
    this.router.navigate(['./']);
  }

  getUser(_id) {
    this.userService.getUserById(_id)
      .then((user) => {
        this.user = user;
      }, (error) => {
        console.log('error', error);
      });
  }
}
