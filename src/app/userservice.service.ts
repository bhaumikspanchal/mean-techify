import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';

import 'rxjs/add/operator/toPromise';
import { LocalStorageService } from 'angular-2-local-storage';

@Injectable()
export class UserService {
  getAllTimeslotUrl = 'service/users';
  getUserByIdUrl = 'service/users';
  authenticateUserUrl = 'service/authenticate';

  
  constructor(
    private http: Http,
    private localStorage: LocalStorageService
    ) { }

  getAllUsers(): Promise<any[]> {
    const token = this.localStorage.get('token');
    const headers = new Headers();
    headers.append('x-access-token', token.toString());
    return this.http.get(this.getAllTimeslotUrl, { headers: headers } )
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }

  getUserById(_id): Promise<any> {
    const token = this.localStorage.get('token');
    const headers = new Headers();
    headers.append('x-access-token', token.toString());
    return this.http.get(this.getUserByIdUrl + '/' + _id , { headers: headers })
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }

  authenticateUser(user: any): Promise<any> {
    return this.http.post(this.authenticateUserUrl, user)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }

  private extractData(res: Response) {
    const body = res.json();
    return body || {};
  }

  private handleError(error: Response | any) {
    return Promise.reject(error.json());
  }

}
