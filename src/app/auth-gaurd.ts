import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, Route } from '@angular/router';
import { LocalStorageService } from 'angular-2-local-storage';

@Injectable()
export class AuthGuard implements CanActivate {


  constructor(
    private _router: Router,
    private localStorage: LocalStorageService
    ) {
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> | boolean {
    const token = this.localStorage.get('token');
    if (token) {
        return true;
    }

    // navigate to login page
    this._router.navigate(['/']);
    return false;
  }

}