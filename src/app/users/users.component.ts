import { Component, OnInit } from '@angular/core';
import { LocalStorageService } from 'angular-2-local-storage';
import { UserService } from 'app/userservice.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css'],
  providers: [ UserService ]
})
export class UsersComponent implements OnInit {

  users:any[];
  errorMessage:any;
  constructor(
    public localStorage: LocalStorageService,
    public userService: UserService,
    private router: Router,
  ) {
   }


  ngOnInit() {
    this.getUsers();
  }

  onLogout() {
    this.localStorage.remove('token');
    this.router.navigate(['./']);
  }

  getUsers() {
    this.userService.getAllUsers()
    .then(res => {
      // console.log('users', res);
      this.users = res;
    });
  }
}
