var express = require('express');
var router = express.Router();
var mongojs = require('mongojs');
var db = mongojs("mongodb://bhaumik:Bhaumik_123@ds161400.mlab.com:61400/dbmean", ["users"]);
var jwt = require('jsonwebtoken');

var secretKey = 'clientsercret';

router.get('/users', function(req, res, next) {
  
  var token = req.headers['x-access-token'];
  if (!token) return res.status(401).send({ auth: false, message: 'No token provided.' });
  
  jwt.verify(token, secretKey, function(err, decoded) {
    if (err) return res.status(500).send({ auth: false, message: 'Failed to authenticate token.' });
    

    db.users.find(function (err, value) {
      res.status(200).send(value);
    })
  });
  

});

router.get('/users/:_id', function(req, res, next) {

  var token = req.headers['x-access-token'];
  if (!token) return res.status(401).send({ auth: false, message: 'No token provided.' });

  jwt.verify(token, secretKey, function(err, decoded) {
    if (err) return res.status(500).send({ auth: false, message: 'Failed to authenticate token.' });
    
    console.log('jwt decoded', decoded);
    db.users.findOne({ _id: mongojs.ObjectId(req.params._id) },function (err, value) {
      // console.log('userfound ', value);
      res.status(200).send(value);
    })
  });
});

router.post('/authenticate',function(req,res,next){
  var body = req.body;
  if(Object.keys(body).length == 0) {
    res.send({error: 'Username or password in not provided.'});
    return;
  }
  // console.log("userdata from request",body);
  db.users.findOne({ email: body.email, password: body.password }, function (err, result) {
    if (err) {
      // console.log('Error updating user: ' + err);
      res.send({'error':'An error has occurred'});
    } else {

      if(!result) {
        return res.status(401).send({auth: false,'message':'Invalid Username or password'});
      }
      // console.log("user got", result);

      var token = jwt.sign({ email: result.email, userType: result.userType }, secretKey, {
        expiresIn: 86400 // expires in 24 hours
      });

      res.send({ auth: true, isAdmin: (result.userType == 'admin' ? true : false), token: token, _id: result._id});
    }
  })
});

module.exports = router;
